﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.CustomerContactManage
{
    [Area("SonicStore")]
    [Authorize(Roles = "sale")]
    public class DetailCustomerContactController : Controller
    {
        private readonly SonicStoreContext _context;

        public DetailCustomerContactController()
        {
            _context = new SonicStoreContext();
        }


        [Route("/contact-detail/{id?}")]
        public async Task<IActionResult> DetailCustomerContactScreen(int id)
        {
            var cus = await (from u in _context.Users
                             join a in _context.Accounts
                             on u.AccountId equals a.Id
                             where u.RoleId == 1 && u.Id == id
                             select new
                             {
                                 AccId = a.Id,
                                 Id = u.Id,
                                 FullName = u.FullName,
                                 Dob = u.Dob,
                                 Email = u.Email,
                                 Phone = u.Phone,
                                 Gender = u.Gender,
                                 UpdateDate = u.UpdateDate,
                                 UpdateBy = u.UpdateBy,
                                 Status = a.Status
                             }).FirstOrDefaultAsync();

            var adds = await _context.UserAddresses.Where(u => u.UserId == id).ToListAsync();
            var checkout = await (from od in _context.OrderDetails
                                  join sto in _context.Storages
                                  on od.StorageId equals sto.Id
                                  join pr in _context.Products
                                  on sto.ProductId equals pr.Id
                                  where od.CustomerId == id && od.Status.Equals("bill")
                                  select new
                                  {
                                      name = pr.Name,
                                      num = od.Quantity,
                                      storage = sto.Storage_capacity,
                                      price = od.Price


                                  }).ToListAsync();
            ViewBag.cus = cus;
            ViewBag.adds = adds;
            ViewBag.checks = checkout;

            return View();
        }
        [HttpPost("/change/{id}")]
        public async Task<IActionResult> changeStatusAccount(int id)
        {

            var acc = await _context.Accounts.Where(u => u.Id == id).FirstOrDefaultAsync();
            var user = _context.Users.FirstOrDefault(u => u.AccountId == acc.Id);
            string name = user.FullName;
            if (acc != null)
            {
                if (acc.Status.Equals("on"))
                {
                    acc.Status = "off";
                    TempData["Message"] = $"Đã khóa tài khoản của {name}";

                }
                else
                {
                    acc.Status = "on";
                    TempData["Message"] = $"Đã mở tài khoản của {name}";

                }
                _context.SaveChanges();

            }
            return RedirectToAction("CustomerContact", "CustomerContact", new { area = "SonicStore" });
        }
    }
}
