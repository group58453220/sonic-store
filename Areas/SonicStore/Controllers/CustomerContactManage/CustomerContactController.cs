﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SonicStore.Areas.SonicStore.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Templates.BlazorIdentity.Pages.Manage;
using static Bogus.DataSets.Name;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.AspNetCore.Authorization;

namespace SonicStore.Areas.SonicStore.Controllers.CustomerContactManage
{
    [Area("SonicStore")]
    [Authorize(Roles = "sale")]
    public class CustomerContactController : Controller
    {
        public CustomerContactController()
        {
            _context = new SonicStoreContext();

        }

        private readonly SonicStoreContext _context;
        [HttpGet("customer-contact")]
        public async Task<IActionResult> CustomerContact()
        {
            var ListC = await (from u in _context.Users
                               join a in _context.Accounts
                               on u.AccountId equals a.Id
                               where u.RoleId == 1
                               select new
                               {
                                   Id = u.Id,
                                   FullName = u.FullName,
                                   Dob = u.Dob,
                                   Email = u.Email,
                                   Phone = u.Phone,
                                   Gender = u.Gender,
                                   UpdateDate = u.UpdateDate,
                                   UpdateBy = u.UpdateBy,
                                   Status = a.Status
                               }).ToListAsync();

            ViewBag.ListC = ListC;
            return View();
        }
    }

}
