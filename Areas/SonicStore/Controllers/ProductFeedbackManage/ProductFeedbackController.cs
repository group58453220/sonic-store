﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.ProductFeedbackManage
{
    [Authorize(Roles = "marketing")]
    [Area("SonicStore")]
    public class ProductFeedbackController : Controller
    {
        private readonly SonicStoreContext _context;

        public ProductFeedbackController(SonicStoreContext context)
        {
            _context = context;
        }
        [HttpGet("product-feedback")]
        public async Task<IActionResult> ProductFeedbackScreen()
        {
            var userJson = HttpContext.Session.GetString("user");
            var userSession = JsonConvert.DeserializeObject<User>(userJson);
            var viewModel = new CompositeViewModel
            {
                UserModel = userSession,

            };
            return View(viewModel);
        }
        [HttpGet("loadData-feedback")]
        public async Task<JsonResult> LoadDataFeedback()
        {
            var listFeedback = await _context.ProductFeedbacks.Include(p => p.Product).Include(p => p.User).Select(p => new
            {
                p.Id,
                p.User.FullName,
                p.Content,
                p.Rating,
                p.Product.Name,
                p.User.Phone,
                p.Status
            }).ToListAsync();
            return Json(new { data = listFeedback });
        }
        [HttpPost("change-status-feedback")]
        public async Task<JsonResult> ChangeStatus(int id)
        {
            var feedback = await _context.ProductFeedbacks.Where(s => s.Id == id).FirstOrDefaultAsync();
            if (feedback != null)
            {
                if (feedback.Status == false)
                {

                    feedback.Status = true;
                }
                else
                {
                    feedback.Status = false;
                }
                _context.ProductFeedbacks.Update(feedback);
            }
            await _context.SaveChangesAsync();
            return Json(new { status = true });
        }
        [HttpPost("view-product-feedback")]
        public async Task<JsonResult> ViewProduct(int id)
        {
            var feedback = await _context.ProductFeedbacks.Where(s => s.Id == id).Select(p => new
            {
                p.Product.Name,
                p.User.FullName,
                p.Content,
                created = p.CreatedDate.ToShortDateString(),
            }).FirstOrDefaultAsync();
            return Json(new { data = feedback });
        }
    }
}
