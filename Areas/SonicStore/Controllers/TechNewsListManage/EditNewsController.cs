﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;
using System.IO;
using System.Threading.Tasks;

namespace SonicStore.Areas.SonicStore.Controllers.TechNewsListManage
{
    [Area("SonicStore")]
    [Authorize(Roles = "marketing")]
    public class EditNewsController : Controller
    {
        private readonly SonicStoreContext _context;
        public EditNewsController(SonicStoreContext context)
        {
            _context = context;
        }

        [Route("edit-news/{id}")]
        public async Task<IActionResult> EditNewsScreen(int id)
        {
            var news = await _context.ProductNews.FindAsync(id);
            ViewBag.News = news;
            ViewBag.ImageFileName = news?.ImageThumbnail != null ? Path.GetFileName(news.ImageThumbnail) : "";
            return View("EditNewsScreen");
        }

        [HttpPost("edit-tech-news")]
        public async Task<IActionResult> EditTechNews([FromForm] ProductNews productNews, [FromForm] string ImageFileName, [FromForm] int Id)
        {
            var userJson = HttpContext.Session.GetString("user");
            User user = null;
            if (!string.IsNullOrEmpty(userJson))
            {
                user = JsonConvert.DeserializeObject<User>(userJson);
            }
            int userId = user.Id;

            if (productNews != null)
            {
                var existingNews = await _context.ProductNews.FindAsync(Id);
                if (existingNews != null)
                {
                    existingNews.Title = productNews.Title;
                    if (!string.IsNullOrEmpty(ImageFileName))
                    {
                        existingNews.ImageThumbnail = "images/tech_news/" + ImageFileName;
                    }
                    existingNews.UpdateAt = DateTime.Now;
                    existingNews.UpdateBy = userId;
                    existingNews.Status = true;
                    existingNews.Content = productNews.Content;

                    await _context.SaveChangesAsync();
                }
            }

            return Redirect("https://localhost:44390/tech-news-list");
        }
    }
}
