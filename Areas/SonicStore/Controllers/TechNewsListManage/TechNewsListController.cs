﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;
using System.Threading.Tasks;
using System.Linq;

namespace SonicStore.Areas.SonicStore.Controllers.TechNewsListManage
{
    [Authorize(Roles = "marketing")]
    [Area("SonicStore")]
    public class TechNewsListController : Controller
    {
        private readonly SonicStoreContext _context;

        public TechNewsListController(SonicStoreContext context)
        {
            _context = context;
        }

        [HttpGet("tech-news-list")]
        public async Task<IActionResult> TechNewsListScreen()
        {
            var listNews = await _context.ProductNews
                .Include(p => p.CreateByNavigation)
                .OrderByDescending(p => p.Id)
                .ToListAsync();

            var userJson = HttpContext.Session.GetString("user");
            var userSession = JsonConvert.DeserializeObject<User>(userJson);

            var viewModel = new CompositeViewModel
            {
                UserModel = userSession,
                NewsList = listNews
            };

            return View(viewModel);
        }

        [HttpPost("TechNewsList/ToggleStatus/{id}")]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> ToggleStatus(int id)
        {
            var news = await _context.ProductNews.FindAsync(id);
            if (news == null)
            {
                return NotFound();
            }

            news.Status = !news.Status;
            _context.Update(news);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
