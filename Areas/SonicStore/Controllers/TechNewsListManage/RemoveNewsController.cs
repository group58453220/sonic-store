﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SonicStore.Areas.SonicStore.Models;
using System.Threading.Tasks;

namespace SonicStore.Areas.SonicStore.Controllers.TechNewsListManage
{
    [Area("SonicStore")]
    [Route("SonicStore/[controller]")]
    [Authorize(Roles = "marketing")]
    public class RemoveNewsController : Controller
    {
        private readonly SonicStoreContext _context;

        public RemoveNewsController(SonicStoreContext context)
        {
            _context = context;
        }

        [HttpPost("RemoveNews/{id}")]
        public async Task<IActionResult> RemoveNews(int id)
        {
            var news = await _context.ProductNews.FindAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            _context.ProductNews.Remove(news);
            await _context.SaveChangesAsync();
            return RedirectToAction("TechNewsListScreen", "TechNewsList");
        }
    }
}
