﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.TechNewsListManage
{
    [Area("SonicStore")]
    [Authorize(Roles = "marketing")]
    public class CreateNewsController : Controller
    {
        private readonly SonicStoreContext _context;
        public CreateNewsController(SonicStoreContext context)
        {
            _context = context;
        }

        [Route("/create-news")]
        public async Task<IActionResult> CreateNewsScreen()
        {

            return View();
        }

        [HttpPost("create-tech-news")]
        public async Task<IActionResult> CreateTechNews([FromForm] ProductNews productNews, [FromForm] string ImageFileName)
        {
            var userJson = HttpContext.Session.GetString("user");
            User user = null;
            if (!string.IsNullOrEmpty(userJson))
            {
                user = JsonConvert.DeserializeObject<User>(userJson);
            }
            int userId = user.Id;
            {
                if (productNews != null)
                {
                    var news = new ProductNews
                    {
                        Title = productNews.Title,
                        CreateAt = DateTime.Now,
                        ImageThumbnail = "images/tech_news/" + ImageFileName,
                        UpdateAt = DateTime.Now,
                        UpdateBy = userId,
                        Status = true,
                        Content = productNews.Content,
                        CreateBy = userId
                    };
                    _context.ProductNews.Add(news);
                    await _context.SaveChangesAsync();

                }
            }

            return Redirect("https://localhost:44390/tech-news-list");
        }

    }
}
