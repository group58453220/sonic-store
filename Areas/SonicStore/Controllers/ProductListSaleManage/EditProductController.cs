﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.ProductListSaleManage
{
    [Area("SonicStore")]
    [Authorize(Roles = "sale")]
    public class EditProductController : Controller
    {

        private readonly SonicStoreContext _context;

        public EditProductController(SonicStoreContext context)
        {
            _context = context;
        }


        [HttpGet]
        [Route("EditCreen/{id}")]
        public async Task<IActionResult> EditCreen(int id)
        {

            var storage = await _context.Storages.FindAsync(id);
            if (storage != null)
            {
                var product = await _context.Products.FindAsync(storage.ProductId);
                if (product != null)
                {
                    var spec = await _context.Specifications.FindAsync(product.Id);
                    var image = await _context.ProductImages.FindAsync(product.Id);

                    ViewBag.product = product;
                    ViewBag.spec = spec;
                    ViewBag.image = image;
                    ViewBag.storage = storage;
                    return View("EditScreen");
                }
            }

            return Redirect("https://localhost:44390/productsSale");
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit([FromForm] Specification spec, [FromForm] Storage storage)
        {
            if (spec != null && storage != null)
            {
                var existingSpec = await _context.Specifications.FindAsync(spec.Id);
                var existingStorage = await _context.Storages.FindAsync(storage.Id);

                if (existingSpec != null && existingStorage != null)
                {
                    bool specChanged = existingSpec.ScreenSize != spec.ScreenSize ||
                     existingSpec.ScreenResolution != spec.ScreenResolution ||
                     existingSpec.Cpu != spec.Cpu ||
                     existingSpec.OperatingSystem != spec.OperatingSystem ||
                     existingSpec.Ram != spec.Ram ||
                     existingSpec.MainCamera != spec.MainCamera ||
                     existingSpec.BatteryCapacity != spec.BatteryCapacity ||
                     existingSpec.FrontCamera != spec.FrontCamera;

                    bool storageChanged = existingStorage.Storage_capacity != storage.Storage_capacity ||
                                          existingStorage.OriginalPrice != storage.OriginalPrice ||
                                          existingStorage.SalePrice != storage.SalePrice ||
                                          existingStorage.quantity != storage.quantity;

                    if (specChanged || storageChanged)
                    {
                        if (specChanged)
                        {
                            existingSpec.ScreenSize = spec.ScreenSize;
                            existingSpec.ScreenResolution = spec.ScreenResolution;
                            existingSpec.Cpu = spec.Cpu;
                            existingSpec.OperatingSystem = spec.OperatingSystem;
                            existingSpec.Ram = spec.Ram;
                            existingSpec.MainCamera = spec.MainCamera;
                            existingSpec.BatteryCapacity = spec.BatteryCapacity;
                            existingSpec.FrontCamera = spec.FrontCamera;
                        }

                        if (storageChanged)
                        {
                            existingStorage.Storage_capacity = storage.Storage_capacity;
                            existingStorage.OriginalPrice = storage.OriginalPrice;
                            existingStorage.SalePrice = storage.SalePrice;
                            existingStorage.quantity = storage.quantity;
                        }

                        await _context.SaveChangesAsync();
                        string id = storage.Id.ToString();
                        TempData["Message"] = $"Đã thay đổi chi tiết sản phẩm";
                        return RedirectToAction("ProductListSale", "ProductListSale", new { area = "SonicStore" });

                    }

                    TempData["Message"] = $"Không có thông tin nào được thay đổi";
                    return RedirectToAction("ProductListSale", "ProductListSale", new { area = "SonicStore" });

                }


            }
            return RedirectToAction("EditCreen");
        }
    }

}