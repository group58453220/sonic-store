﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.SliderManage
{
	[Authorize(Roles = "marketing")]
	[Area("SonicStore")]
	public class SliderController : Controller
	{
		private readonly SonicStoreContext _context;

		public SliderController(SonicStoreContext context)
		{
			_context = context;
		}
		public class userInput
		{
			public string title { get; set; }
			public string backlink { get; set; }
			public string image { get; set; }
		}
		[HttpGet("slider-manage")]
		public async Task<IActionResult> SliderScreen()
		{
			var userJson = HttpContext.Session.GetString("user");
			var userSession = JsonConvert.DeserializeObject<User>(userJson);
			var viewModel = new CompositeViewModel
			{
				UserModel = userSession,

            };
            return View(viewModel);
        }
        [HttpGet("loadData-slider")]
        public async Task<JsonResult> loadData()
        {
            var listSlider = await _context.Sliders.Select(s => new
            {
                s.Id,
                s.Title,
                s.Backlink,
                s.Image,
                s.User.FullName,
                s.Status,
            }).ToListAsync();
            return Json(new { data = listSlider });
        }
        [HttpPost("change-status-slider")]
        public async Task<JsonResult> ChangeStatus(int id)
        {
            int status = 1;
            var slider = await _context.Sliders.Where(s => s.Id == id).FirstOrDefaultAsync();
            var sliderOn = await _context.Sliders.Where(s => s.Status == "on").CountAsync();
            var sliderOff = await _context.Sliders.Where(s => s.Status == "off").CountAsync();
            
            if (slider != null)
            {
                if (slider.Status == "on")
                {
                    if ( sliderOn < 5)
                    {
                        status = 2;
                        slider.Status = "off";
                        _context.Sliders.Update(slider);
                        await _context.SaveChangesAsync();
                        return Json(new { status = status });
                    }
                    slider.Status = "off";
                    _context.Sliders.Update(slider);

                }
                else
                {
                    if (sliderOn >= 5 )
                    {
                        status = 3;
                        slider.Status = "on";
                        _context.Sliders.Update(slider);
                        await _context.SaveChangesAsync();
                        return Json(new { status = status });
                    }
                    slider.Status = "on";
                    _context.Sliders.Update(slider);
                }
            }
            await _context.SaveChangesAsync();
            return Json(new { status = status });
        }
        [HttpPost("create-slider")]
        public async Task<JsonResult> CreateSlider(string strSlider)
        {
            var userJson = HttpContext.Session.GetString("user");
            var userSession = JsonConvert.DeserializeObject<User>(userJson);
            var sliderJson = JsonConvert.DeserializeObject<userInput>(strSlider);
            var slider = new Slider
            {
                Title = sliderJson.title,
                UserId = userSession.Id,
                Backlink = sliderJson.backlink,
                Image = sliderJson.image,
                Status = "off"

			};
			_context.Sliders.Add(slider);
			await _context.SaveChangesAsync();
			return Json(new { status = true });
		}
		[HttpPost("remove-slider")]
		public async Task<JsonResult> RemoveSlider(int id)
		{
			var status = true;
			var slider = await _context.Sliders.Where(s => s.Id == id).FirstOrDefaultAsync();
			var sliderNumber = await _context.Sliders.CountAsync();
			if (sliderNumber == 5)
			{
				status = false;
				return Json(new { status = true });
			}
			_context.Sliders.Remove(slider);
			await _context.SaveChangesAsync();
			return Json(new { status = status });
		}
		[HttpPost("edit-slider")]
		public async Task<JsonResult> EditSlider(string strSlider, int id)
		{
			try
			{

				var userJson = HttpContext.Session.GetString("user");
				var userSession = JsonConvert.DeserializeObject<User>(userJson);
				var slider = await _context.Sliders.Where(s => s.Id == id).FirstOrDefaultAsync();
				var sliderJson = JsonConvert.DeserializeObject<userInput>(strSlider);
				if (sliderJson != null)
				{
					slider.Title = sliderJson.title;
					slider.Backlink = sliderJson.backlink;
					slider.Image = sliderJson.image;
					_context.Sliders.Update(slider);

				}
				await _context.SaveChangesAsync();
			}
			catch (Exception ex)
			{

			}
			return Json(new { status = true });
		}
		[HttpPost("view-slider")]
		public async Task<JsonResult> ViewSlider(int id)
		{
			var slider = await _context.Sliders.Where(s => s.Id == id).Select(s => new
			{
				s.Title,
				s.Backlink,
				s.Image
			}).FirstOrDefaultAsync();
			return Json(new { data = slider });
		}
	}
}
