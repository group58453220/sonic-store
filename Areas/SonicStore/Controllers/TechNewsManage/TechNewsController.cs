﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SonicStore.Areas.SonicStore.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SonicStore.Areas.SonicStore.Controllers.ProductNewsManage
{
    [Area("SonicStore")]
    public class TechNewsController : Controller
    {
        private readonly SonicStoreContext _context;

        public TechNewsController(SonicStoreContext context)
        {
            _context = context;
        }

        [HttpGet("tech-news")]
        public async Task<IActionResult> TechNewsScreen(int page = 1, int pageSize = 6)
        {
            var pagedData = await GetPagedNews(page, pageSize);
            var totalRow = await _context.ProductNews.CountAsync();
            var totalPages = (int)Math.Ceiling((double)totalRow / pageSize);

            ViewBag.CurrentPage = page;
            ViewBag.TotalPages = totalPages;

            return View("TechNewsScreen", pagedData);
        }

        private async Task<List<ProductNews>> GetPagedNews(int page, int pageSize)
        {
            return await _context.ProductNews
                .Include(p => p.CreateByNavigation)
                .OrderByDescending(p => p.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
    }
}
