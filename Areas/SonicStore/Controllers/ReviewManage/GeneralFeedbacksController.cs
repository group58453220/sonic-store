﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;
using System.Threading.Tasks;

namespace SonicStore.Areas.SonicStore.Controllers.ReviewManage
{
    [Authorize(Roles ="customer")]
    public class GeneralFeedbacksController : Controller
    {
        private readonly SonicStoreContext _storeContext;

        public GeneralFeedbacksController(SonicStoreContext context)
        {
            _storeContext = context;
        }

        [HttpPost("general-feedback/add")]
        public async Task<bool> AddFeedback(string content, string title, DateTime created_date)
        {
            try
            {
                // Retrieve user information from session
                var userJson = HttpContext.Session.GetString("user") ?? "";

                // Check if userJson is not null or empty
                if (string.IsNullOrEmpty(userJson)) throw new Exception("User is not logged in");

                // Deserialize JSON string to User object
                var user = JsonConvert.DeserializeObject<User>(userJson);

                // Use DateTime.Now or DateTime.UtcNow if created_date is not provided or is the default value
                var feedbackCreatedDate = (created_date == DateTime.MinValue) ? DateTime.Now : created_date;


                // Add feedback to the context
                _storeContext.GeneralFeedbacks.Add(new GeneralFeedback
                {
                    Content = content,
                    Title = title,
                    Status = false,
                    UserId = user?.Id ?? 0,
                    CreatedDate = feedbackCreatedDate
                });

                // Save changes to the database
                return await _storeContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                // Log the exception (you can log this to a file, database, or any logging service)
                Console.WriteLine(ex.Message);
                // Handle the error as needed
            }

            return false;
        }


        [HttpGet("general-feedback/add")]
        public IActionResult AddFeedback()
        {
            // Retrieve user information from session
            var userJson = HttpContext.Session.GetString("user") ?? "";

            // Check if userJson is not null or empty
            if (string.IsNullOrEmpty(userJson))
            {
                return Redirect("/SonicStore/Login/login");
            }
            return ViewComponent("GeneralFeedback");
        }
    }
}
