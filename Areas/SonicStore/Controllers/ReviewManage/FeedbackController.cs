﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.ReviewManage
{
    [Area("SonicStore")]
    public class FeedbackController : Controller
    {
        private readonly ILogger<FeedbackController> _logger;
        private readonly SonicStoreContext _storeContext;

        public FeedbackController(ILogger<FeedbackController> logger, SonicStoreContext storeContext)
        {
            _logger = logger;
            _storeContext = storeContext;
        }

        [HttpGet("feedback")]
        public IActionResult FeedbackView(int productId) => ViewComponent("Feedback", new { productId });

        [HttpPost("feedback/add")]
        public async Task<bool> AddFeedback(int rating, string content, int productId, DateTime created_date)
        {
            try
            {
                // Retrieve user information from session
                var userJson = HttpContext.Session.GetString("user") ?? "";

                // Check if userJson is not null or empty
                if (string.IsNullOrEmpty(userJson)) throw new Exception();

                // Deserialize JSON string to User object
                var user = JsonConvert.DeserializeObject<User>(userJson);
                var feedbackCreatedDate = (created_date == DateTime.MinValue) ? DateTime.Now : created_date;
                _storeContext.ProductFeedbacks.Add(new ProductFeedback
                {
                    Content = content,
                    Rating = rating,
                    UserId = user?.Id ?? 0,
                    ProductId = productId,
                    CreatedDate = feedbackCreatedDate
                });

                return await _storeContext.SaveChangesAsync() > 0;
            }
            catch
            {
                return false;
            }
        }

    }
}



