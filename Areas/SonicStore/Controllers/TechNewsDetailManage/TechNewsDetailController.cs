﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Controllers.TechNewsDetailManage
{
    [Area("SonicStore")]
    [Authorize(Roles = "marketing")]
    public class TechNewsDetailController : Controller
    {

        private readonly SonicStoreContext _context;
        public TechNewsDetailController(SonicStoreContext context)
        {
            _context = context;
        }
        [HttpGet("tech-news-detail/{id?}")]
        public async Task<IActionResult> TechNewsDetailScreen(int? id)
        {
            var newsDetail = await _context.ProductNews.Include(p => p.CreateByNavigation).FirstOrDefaultAsync(p => p.Id == id);


            ViewBag.NewsDetail = newsDetail;
            return View("TechNewsDetailScreen");
        }
    }
}
