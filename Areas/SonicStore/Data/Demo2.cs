﻿using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.Areas.SonicStore.Data
{
    public class Demo2
    {


        public static async Task InsertBrand(string name, string image)
        {
            using (var context = new SonicStoreContext())
            {
                await context.AddAsync(new Brand
                {
                    BrandName = name,
                    brandImage = image
                });
                await context.SaveChangesAsync();
            }
        }




        static async Task Main(string[] args)
        {
            string name;
            string image;
            Console.WriteLine("Enter name: ");
            name = Console.ReadLine();
            Console.WriteLine("Enter image: ");
            image = Console.ReadLine();
            InsertBrand(name, image);

        }
    }




}
