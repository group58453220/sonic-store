﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SonicStore.Areas.SonicStore.Models
{

    [Table("Specification")]
    public class Specification
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("screen_size")]
        public double ScreenSize { get; set; }
        [Column("screen_resolution")]
        [StringLength(50)]
        public string ScreenResolution { get; set; }
        [Column("cpu")]
        [StringLength(50)]
        public string Cpu { get; set; }
        [Column("operating_system")]
        [StringLength(50)]
        public string OperatingSystem { get; set; }
        [Column("ram")]
        public int Ram { get; set; }
        [Column("main_camera")]
        public int MainCamera { get; set; }
        [Column("battery_capacity")]
        public int BatteryCapacity { get; set; }
        [Column("front_camera")]
        public int FrontCamera { get; set; }
        [Column("product_id")]
        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; } = null!;
    }
}
