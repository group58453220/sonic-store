﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SonicStore.Areas.SonicStore.Models;

[PrimaryKey("Id", "UserId")]
[Table("General_Feedback")]
public partial class GeneralFeedback
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("content")]
    [StringLength(3000)]
    public string? Content { get; set; }
    [Column("title")]
    public string? Title { get; set; }
    [Column("status")]
    public bool? Status { get; set; }

    [Column("created_date")]
    public DateTime? CreatedDate { get; set; }
    [Key]
    [Column("user_id")]
    public int UserId { get; set; }

    [ForeignKey("UserId")]
    [InverseProperty("GeneralFeedbacks")]
    public virtual User User { get; set; } = null!;
}
