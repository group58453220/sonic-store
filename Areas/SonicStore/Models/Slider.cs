﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SonicStore.Areas.SonicStore.Models;

[Table("Slider")]
public partial class Slider
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("title")]
    [StringLength(1000)]
    public string? Title { get; set; }

    [Column("image")]
    [StringLength(225)]
    public string? Image { get; set; }

    [Column("back_link")]
    [StringLength(1000)]
    public string? Backlink { get; set; }

    [Column("status")]
    [StringLength(50)]
    public string? Status { get; set; }

    [Column("note")]
    public string? Note { get; set; }

    [Column("user_id")]
    public int UserId { get; set; }

    [ForeignKey("UserId")]
    [InverseProperty("Sliders")]
    public virtual User User { get; set; } = null!;
}
