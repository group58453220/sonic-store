﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SonicStore.Areas.SonicStore.Models;

[Table("Tech_News")]
public partial class ProductNews
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("title")]
    [StringLength(2000)]
    public string? Title { get; set; }

    [Column("create_at", TypeName = "datetime")]
    public DateTime? CreateAt { get; set; }

    [Column("image_thumbnail")]
    [StringLength(200)]
    [Unicode(false)]
    public string? ImageThumbnail { get; set; }

    [Column("update_at", TypeName = "datetime")]
    public DateTime? UpdateAt { get; set; }

    [Column("update_by")]
    public int? UpdateBy { get; set; }

    [Column("status")]
    public bool? Status { get; set; }

    [Column("content")]
    public string? Content { get; set; }

    [Column("create_by")]
    public int CreateBy { get; set; }

    [ForeignKey("CreateBy")]
    [InverseProperty("ProductNews")]
    public virtual User CreateByNavigation { get; set; } = null!;
}
