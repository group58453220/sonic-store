﻿using Microsoft.AspNetCore.Mvc;

namespace SonicStore.Areas.SonicStore.Models
{
    public class CompositeViewModel
    {
        [BindProperty]
        public Account AccountModel { get; set; }
        public User UserModel { get; set; }
        public UserAddress UserAddress { get; set; }
        public List<ProductNews> NewsList { get; set; }
    }
}
