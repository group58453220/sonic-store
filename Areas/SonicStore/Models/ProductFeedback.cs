﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SonicStore.Areas.SonicStore.Models;

[PrimaryKey("Id", "ProductId", "UserId")]
[Table("Product_Feedback")]
public partial class ProductFeedback
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("content")]
    [StringLength(3000)]
    public string Content { get; set; } = null!;

    [Column("rating")]
    public int? Rating { get; set; }

    [Key]
    [Column("product_id")]
    public int ProductId { get; set; }

    [Column("Status")]
    public bool Status {  get; set; }
    [Column("created_date")]
    public DateTime CreatedDate { get; set; }
    [Key]
    [Column("user_id")]
    public int UserId { get; set; }

    [ForeignKey("ProductId")]
    [InverseProperty("ProductFeedbacks")]
    public virtual Product Product { get; set; } = null!;

    [ForeignKey("UserId")]
    [InverseProperty("ProductFeedbacks")]
    public virtual User User { get; set; } = null!;
}
