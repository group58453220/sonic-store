﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SonicStore.Areas.SonicStore.Controllers.ReviewManage;
using SonicStore.Areas.SonicStore.Models;

namespace SonicStore.ViewComponents
{
    [ViewComponent(Name = "Feedback")]
    public class FeedbackViewComponent : ViewComponent
    {
        private readonly ILogger<FeedbackController> _logger;
        private readonly SonicStoreContext _storeContext;

        public FeedbackViewComponent(ILogger<FeedbackController> logger, SonicStoreContext storeContext)
        {
            _logger = logger;
            _storeContext = storeContext;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? productId)
        {
            try
            { 
                // Retrieve user information from session
                var userJson = HttpContext.Session.GetString("user");

                // Deserialize JSON string to User object
                var user = JsonConvert.DeserializeObject<User>(userJson ?? "");

                var userId = user?.Id ?? 0;
                var products = await _storeContext.Products.FirstOrDefaultAsync(p => p.Id == productId);
                var feedbacks = await _storeContext.ProductFeedbacks.Include(x => x.User).Include(x => x.Product).Where(x => x.ProductId == productId).ToListAsync();


                var purchased =
                   (from customer in _storeContext.Users
                    join cart in _storeContext.OrderDetails on customer.Id equals cart.CustomerId
                    join storage in _storeContext.Storages on cart.StorageId equals storage.Id
                    join checkout in _storeContext.Orders on cart.Id equals checkout.CartId
                    join pay in _storeContext.Payments on checkout.PaymentId equals pay.Id
                    join payStatus in _storeContext.StatusPayments on pay.Id equals payStatus.Payment_id
                    where customer.Id == userId 
                    && storage.ProductId == productId 
                    && (payStatus.Type ??"").ToLower() == "Đã thanh toán".ToLower()
                    && (cart.Status ?? "").ToLower() == "bill".ToLower()
                    select new
                    {
                        customerId = customer.Id,
                        productId = cart.StorageId
                    }).Any();


                ViewBag.Product = products;
                ViewBag.Purchased = purchased;
                ViewBag.Feedback = feedbacks;
                ViewBag.ProductId = productId;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View();
        }
    }
    
}
