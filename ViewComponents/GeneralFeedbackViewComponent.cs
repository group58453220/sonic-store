﻿using Microsoft.AspNetCore.Mvc;

namespace SonicStore.ViewComponents
{
    [ViewComponent(Name = "GeneralFeedback")]
    public class GeneralFeedbackViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke() => View();
    }
}
