﻿namespace SonicStore.Models
{
    public class UpdateOrderModel
    {
        public int OrderId { get; set; }

        public int Quantity { get; set; }
    }
}
